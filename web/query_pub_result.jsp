<%@ page import="java.util.ArrayList" %>
<%@ page import="org.eclipse.rdf4j.model.Model" %>
<%@ page import="it.bice.GestoreRepo" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.Scanner" %><%--
  Created by IntelliJ IDEA.
  User: beart
  Date: 2/19/2020
  Time: 4:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="query_aut_style.css">

</head>
<body>
<center>

    <div class="topnav">
        <a href="index.html">Home</a>
        <a href="update.html">Update</a>
        <a class="active" href="query.html">Query</a>
    </div>

    <h1>
        Query Results
    </h1>

        <div id="pub-div">
            <form method="post" action="Query">
                <br><br>
                <label> Publicaiton Name:
                    ${queryRes.get("Name")}
                </label>
            </form>
        </div>

        <div id="author-div">
            <form method="post" action="Query">
            <br><br>
            <label> Author Name:
                <input name="Author" value="${queryRes.get("Author")}" style="display:none;">
                <input type="submit" value="${queryRes.get("Author")}">
            </label>
            </form>
        </div>

        <div id="year-div">
            <form method="post" action="Query">
                <br><br>
                <label> Publication Year:
                    <input name="Year" value="${queryRes.get("PublicationYear")}" style="display:none;">
                    <input type="submit" value="${queryRes.get("PublicationYear")}">
                </label>
            </form>
        </div>




        <%--<%

            ArrayList<Model> result = (ArrayList<Model>) request.getAttribute("query");
            Model query = (Model) result.get(0);
            GestoreRepo.printQuery(query);

            File output = new File("D:/IdeaProjectUltimate/WebAppTest4/resources/newoutput.txt");
            if(output.length()!=0){
                Scanner myReader = new Scanner(output);
                while (myReader.hasNextLine()) {
                    String data = myReader.nextLine();
                    out.println(data+"<br/>");
                }
                myReader.close();
            }
            else{
                out.print("NO QUERY RESULTS FOUND");
            }
            output.delete();

        %>--%>
</center>
</body>
</html>
