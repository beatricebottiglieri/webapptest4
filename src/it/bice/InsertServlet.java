package it.bice;

import it.bice.model.Publication;
import org.eclipse.rdf4j.model.Model;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;


@WebServlet(
        name = "insertservlet",
        urlPatterns = "/InsertPublication"
)
public class InsertServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int insert = insertData(req);

        String msg = null;
        if(insert == 0) {
            msg = "Inserimento avvenuto con successo!";
        }
        else if(insert == -1){
            msg = "Inserimento fallito!";
        }
        else if(insert == -2){
            msg = "Inserimento avvenuto con successo (mancante autore)!";
        }

        ArrayList out = new ArrayList();
        out.add(msg);
        req.setAttribute("message", out);
        RequestDispatcher view = req.getRequestDispatcher("insert_result.jsp");
        view.forward(req, resp);

    }
     protected int insertData(HttpServletRequest req){

         GestoreRepo gestoreRepo = new GestoreRepo();
         Model queryUpdate = null;

         String aut_string = null;
         String name_string = null;
         String isbn_string = null;

         req.getParameterMap().forEach((key, value) -> System.out.println(key + ":" + Arrays.asList(value)));

         for (Map.Entry entry : req.getParameterMap().entrySet()) {
             String type = (String) entry.getKey();

             String[] aut_array = null;
             String[] name_array = null;
             String[] date_array = null;
             String[] isbn_array = null;

             if(type.equals("Novel")||type.equals("MusicalComp")||type.equals("Play")
                     ||type.equals("Poem")||type.equals("Screenplay")||type.equals("ShortStory")) {

                 name_array = (String[]) entry.getValue();
                 System.out.println(Arrays.asList(name_array));
                 if (name_array[0] != "") {
                     System.out.println(type + " NOT NULL");
                     System.out.println(Arrays.asList(name_array));
                     System.out.println(name_array[0]);
                     name_string = name_array[0];

                     queryUpdate = gestoreRepo.askQuery(name_string, null, null);
                     if (queryUpdate.isEmpty()) {
                         gestoreRepo.insertStatement(name_string, null, type);
                         gestoreRepo.insertData(name_string, "title", name_string);
                     }

                     if (queryUpdate == null) {
                         gestoreRepo.closeRepo();
                         return -1;
                     }
                     queryUpdate = null;

                 }
             }

             else if(type.equals("isbn-num")){

                 isbn_array= (String[]) entry.getValue();
                 if(isbn_array[0]!="") {
                     isbn_string=isbn_array[0];
                     queryUpdate = gestoreRepo.askQuery(name_string, null, null);
                     if (!queryUpdate.isEmpty()) {
                         gestoreRepo.insertStatement(name_string, "hasManifestation", name_string + "/"+isbn_string);
                     }
                 }
             }


             else if (type.equals("autname")){

                 aut_array = (String[]) entry.getValue();
                 if(aut_array[0]!="") {
                     aut_string = aut_array[0];
                 }

             } else if (type.equals("autsurname")){
                 System.out.println("cognome");
                 aut_array = (String[]) entry.getValue();
                 System.out.println(aut_array[0]);
                 if(aut_array[0]!="") {
                     aut_string = aut_string + " " + aut_array[0];
                     gestoreRepo.insertStatement(name_string, "creator", aut_string);
                     gestoreRepo.insertStatement(aut_string, null, "Person");
                     queryUpdate = gestoreRepo.askQuery(aut_string, null, null);
                 }

                 if(queryUpdate==null){
                     gestoreRepo.closeRepo();
                     return -2;
                 }
                 queryUpdate=null;

                 aut_string=null;

             } else if (type.equals("publication-date")){
                 System.out.println("STO NELLA DATA");
                 String date = null;
                 date_array = (String[]) entry.getValue();
                 System.out.println(Arrays.asList(date_array[0]));
                 if(date_array[0]!="") {
                     date = date_array[0];
                     System.out.println(date);
                     if(date.length()==4){
                         gestoreRepo.insertData(name_string, "hasPublicationYear", date );
                     }
                     else {
                         gestoreRepo.insertData(name_string, "publicationDate", date);
                     }
                 }

                 //queryUpdate = gestoreRepo.askQuery(aut_string, null, null);
                 //if(queryUpdate==null){
                 //    return -1;
                 //}
                 queryUpdate=null;
                 name_string=null;
                 break;
             }


         }
         gestoreRepo.closeRepo();
         return 0;


     }


}