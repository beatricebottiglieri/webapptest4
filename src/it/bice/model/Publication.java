package it.bice.model;

public enum Publication {
    Novel,Poem,ShortStory,Play,MusicalComp,Screenplay;
}
