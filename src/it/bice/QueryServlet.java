package it.bice;

import it.bice.model.Publication;
import jdk.internal.cmm.SystemResourcePressureImpl;
import org.eclipse.rdf4j.model.Model;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;


@WebServlet(
        name = "queryservlet",
        urlPatterns = "/Query"
)
public class QueryServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);

        GestoreRepo gestoreRepo = new GestoreRepo();
        Model queryUpdate = null;
        HashMap out_map = null;
        ArrayList out_list = null;
        RequestDispatcher view = null;



        for (Map.Entry entry : req.getParameterMap().entrySet()) {

            String type = (String) entry.getKey();
            System.out.println("CURRENT TYPE IS "+type);
            String[] array = null;

            if (type.equals("Publication")) {
                array = (String[]) entry.getValue();
                if (array[0] != "") {
                    queryUpdate = gestoreRepo.askQuery(array[0], null, null);
                    out_map = gestoreRepo.getStatementMap(queryUpdate);
                    req.setAttribute("queryType", "Pub");
                    req.setAttribute("queryRes", out_map);
                    view = req.getRequestDispatcher("query_pub_result.jsp");
                    break;

                }
            }else if (type.equals("Author")) {
                array = (String[]) entry.getValue();
                if (array[0] != "") {
                    System.out.println("AUTHOR");
                    queryUpdate = gestoreRepo.askQuery(null, null, array[0]);
                    out_list = gestoreRepo.getStatementsList(queryUpdate);
                    System.out.println(out_list);
                    req.setAttribute("queryType", "Author");
                    req.setAttribute("queryRes", out_list);
                    view = req.getRequestDispatcher("query_aut_result.jsp");
                    break;
                }
            }
            else if(type.equals("Year")){
                array = (String[]) entry.getValue();
                if (array[0] != "") {
                    queryUpdate = gestoreRepo.askQuery( null, "hasPublicationYear", array[0]);
                    out_list = gestoreRepo.getStatementsList(queryUpdate);
                    System.out.println(out_list);
                    req.setAttribute("queryType", "Author");
                    req.setAttribute("queryRes", out_list);
                    view = req.getRequestDispatcher("query_aut_result.jsp");
                    break;
                }
            }

        }

        gestoreRepo.closeRepo();

        view.forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        GestoreRepo gestoreRepo = new GestoreRepo();
        Model queryUpdate = null;
        HashMap out_map = null;
        ArrayList out_list = null;
        RequestDispatcher view = null;



        for (Map.Entry entry : req.getParameterMap().entrySet()) {

                String type = (String) entry.getKey();
                System.out.println("CURRENT TYPE IS "+type);
                String[] array = null;

                if (type.equals("Publication")) {
                    array = (String[]) entry.getValue();
                    if (array[0] != "") {
                        System.out.println(array[0]);
                        queryUpdate = gestoreRepo.askQuery(array[0], null, null);
                        out_map = gestoreRepo.getStatementMap(queryUpdate);
                        req.setAttribute("queryType", "Pub");
                        req.setAttribute("queryRes", out_map);
                        view = req.getRequestDispatcher("query_pub_result.jsp");
                        break;

                    }
                }else if (type.equals("Author")) {
                        array = (String[]) entry.getValue();
                        if (array[0] != "") {
                            System.out.println("AUTHOR");
                            queryUpdate = gestoreRepo.askQuery(null, null, array[0]);
                            out_list = gestoreRepo.getStatementsList(queryUpdate);
                            System.out.println(out_list);
                            req.setAttribute("queryType", "Author");
                            req.setAttribute("queryRes", out_list);
                            view = req.getRequestDispatcher("query_aut_result.jsp");
                            break;
                        }
                }
                else if(type.equals("Year")){
                    array = (String[]) entry.getValue();
                    if (array[0] != "") {
                        System.out.println(array[0]);
                        queryUpdate = gestoreRepo.askDateQuery( null, "hasPublicationYear", array[0]);
                        System.out.println(queryUpdate);
                        out_list = gestoreRepo.getStatementsList(queryUpdate);
                        System.out.println(out_list);
                        req.setAttribute("queryType", "Author");
                        req.setAttribute("queryRes", out_list);
                        view = req.getRequestDispatcher("query_aut_result.jsp");
                        break;
                    }
                }

            }

            gestoreRepo.closeRepo();

            view.forward(req, resp);


        }

    }

