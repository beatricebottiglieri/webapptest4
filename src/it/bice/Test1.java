package it.bice;

import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.*;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.eclipse.rdf4j.sail.Sail;
import org.eclipse.rdf4j.sail.SailException;
import org.eclipse.rdf4j.sail.inferencer.fc.SchemaCachingRDFSInferencer;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class Test1 {

    public Repository rep = null;



    public Test1() {

    }

    public void createRepo(){
        File file1 = new File("D:/IdeaProjectUltimate/WebAppTest4/repository");
        //Creating the directory
        boolean bool = file1.mkdir();
        MemoryStore memoryStore = new MemoryStore();
        memoryStore.setPersist(true);
        memoryStore.setDataDir(file1);
        SchemaCachingRDFSInferencer schemaCachingRdfsInferencer = new SchemaCachingRDFSInferencer(
                memoryStore);

        Repository rep = null;
        rep = new SailRepository(schemaCachingRdfsInferencer);
        try{
            try {

                rep.init();
            } catch (RepositoryException re) {
                System.out.println("INIT FAILED MANNAGGIA A DIO");
            }

            File file = new File("C:/Users/beart/OneDrive - Universita' degli Studi di Roma Tor Vergata/IA2 PROJECT/fabio-master/docs/current/fabio.ttl");
            String baseURI = "http://purl.org/spar/fabio/";

            try (RepositoryConnection con = rep.getConnection()) {
                try {
                    con.add(file, baseURI, RDFFormat.TURTLE);
                } finally {
                    con.close();
                }
            } catch (RDF4JException | IOException e) {
                System.out.println("PORCODIO");

            }
        } finally {
            rep.shutDown(); // spengo il repository
        }

    }


    public List generateTriple(String s) throws RDFHandlerException, UnsupportedRDFormatException,
            URISyntaxException {
        File file1 = new File("D:/IdeaProjectUltimate/WebAppTest4/repository");
        //Creating the directory
        boolean bool = file1.mkdir();
        MemoryStore memoryStore = new MemoryStore();
        memoryStore.setPersist(true);
        memoryStore.setDataDir(file1);
        SchemaCachingRDFSInferencer schemaCachingRdfsInferencer = new SchemaCachingRDFSInferencer(
                memoryStore);

        Repository rep = null;
        rep = new SailRepository(schemaCachingRdfsInferencer);
        try{
            try {

                rep.init();
            } catch (RepositoryException re) {
                System.out.println("INIT FAILED MANNAGGIA A DIO");
            }

            File file = new File("C:/Users/beart/OneDrive - Universita' degli Studi di Roma Tor Vergata/IA2 PROJECT/fabio-master/docs/current/fabio.ttl");
            String baseURI = "http://purl.org/spar/fabio/";

            try (RepositoryConnection con = rep.getConnection()) {
                try {
                    con.add(file, baseURI, RDFFormat.TURTLE);
                } finally {
                    con.close();
                }
            } catch (RDF4JException | IOException e) {
                System.out.println("PORCODIO");

            }

            try (RepositoryConnection con = rep.getConnection()) {
                return QueryResults.asList(con.getStatements(
                        con.getValueFactory().createIRI("http://purl.org/spar/fabio/"+s), null, null, false));
            }
        } finally {
            rep.shutDown(); // spengo il repository
        }
    }

}



