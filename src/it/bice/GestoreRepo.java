package it.bice;

import org.eclipse.rdf4j.model.*;
import org.eclipse.rdf4j.model.impl.SimpleIRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.query.*;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.BasicWriterSettings;
import org.eclipse.rdf4j.sail.inferencer.fc.SchemaCachingRDFSInferencer;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sail.memory.model.MemIRI;

import javax.servlet.jsp.JspWriter;
import javax.swing.plaf.nimbus.State;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GestoreRepo {

    private Repository rep = null;

    public GestoreRepo() {
        File folder = new File("D:/IdeaProjectUltimate/WebAppTest4/repository");
        boolean exists = folder.exists();

        MemoryStore memoryStore = null;
        SchemaCachingRDFSInferencer schemaCachingRdfsInferencer = null;


        if (exists == false) {
            //Creating the directory
            boolean bool = folder.mkdir();

            memoryStore = new MemoryStore(folder);
            memoryStore.setPersist(true);
            schemaCachingRdfsInferencer = new SchemaCachingRDFSInferencer(
                    memoryStore);

            rep = new SailRepository(schemaCachingRdfsInferencer);
            rep.init();
            File fabio= new File("D:/IdeaProjectUltimate/WebAppTest4/resources/fabio.ttl");
            String baseURI = "http://purl.org/spar/fabio/";

            try (RepositoryConnection con = rep.getConnection()) {
                try {
                    con.add(fabio, baseURI, RDFFormat.TURTLE);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    con.close();
                }
            }

        }
        else{
            memoryStore = new MemoryStore(folder);
            memoryStore.setPersist(true);
            schemaCachingRdfsInferencer = new SchemaCachingRDFSInferencer(memoryStore);

            rep = new SailRepository(schemaCachingRdfsInferencer);
            rep.init();
        }

    }



    //only if objects is a resource
    public Model askQuery(String subject, String predicate, String object){

        Model queryRes = null;
        String base = "http://purl.org/spar/fabio/";
        String terms = "http://purl.org/dc/terms/";


        try(RepositoryConnection repCon = rep.getConnection()) {

            IRI subj = null;
            IRI pred = null;
            IRI obj = null;

            if (subject != null) {
                String s = null;
                s = base + getIRICase(subject);
                subj = repCon.getValueFactory().createIRI(s);
            }
            if (predicate != null) {
                String p = null;
                if(predicate.equals("creator")){
                    p = terms + predicate;
                    pred = repCon.getValueFactory().createIRI(p);
                }
                else if(predicate.equals("hasPublicationYear")){
                    p = base + predicate;
                    pred = repCon.getValueFactory().createIRI(p);
                }
                else {
                    p = base + getIRICase(predicate);
                    pred = repCon.getValueFactory().createIRI(p);
                }
            }
            if (object != null) {
                String o = null;
                o = base + getIRICase(object);
                obj = repCon.getValueFactory().createIRI(o);
            }

            //System.out.println(pred);

            //true per inferenza
            queryRes = QueryResults.asModel(repCon.getStatements(
                    subj,
                    pred,
                    obj,
                    true));

            repCon.close();
        }
        return queryRes;

    }

    //only if objects is a date
    public Model askDateQuery(String subject, String predicate, String object){

        Model queryRes = null;
        Model queryOut = new TreeModel();
        String base = "http://purl.org/spar/fabio/";
        String terms = "http://purl.org/dc/terms/";
        String xsd = "http://www.w3.org/2001/XMLSchema";


        try(RepositoryConnection repCon = rep.getConnection()) {

            IRI subj = null;
            IRI pred = null;
            IRI obj = null;
            Value objvalue = null;

            if (subject != null) {
                String s = null;
                s = base + getIRICase(subject);
                subj = repCon.getValueFactory().createIRI(s);
            }
            if (predicate != null) {
                String p = null;
                if(predicate.equals("creator")){
                    p = terms + predicate;
                    pred = repCon.getValueFactory().createIRI(p);
                }
                else if(predicate.equals("hasPublicationYear")){
                    p = base + predicate;
                    pred = repCon.getValueFactory().createIRI(p);
                }
                else {
                    p = base + getIRICase(predicate);
                    pred = repCon.getValueFactory().createIRI(p);
                }
            }

            //true per inferenza
            queryRes = QueryResults.asModel(repCon.getStatements(
                    null,
                    pred,
                    null,
                    true));

            Value v = null;
            String year = null;
            for(Statement st:queryRes){

                if(st!=null){
                    v = st.getObject();
                    year=v.stringValue();
                    if(year.equals(object)) {
                        queryOut.add(st);
                    }
                }

            }

            repCon.close();
        }
        return queryOut;

    }



    public ArrayList getStatementsList(Model model){

        String fabio = "http://purl.org/spar/fabio/";
        String dcterms = "http://purl.org/dc/terms/";
        model.setNamespace("fabio", fabio);
        model.setNamespace("dcterms",dcterms);

        ValueFactory vf = SimpleValueFactory.getInstance();

        IRI title = vf.createIRI(dcterms,"title");

        ArrayList out = new ArrayList();
        String name = null;

        Model queryModel = null;

        try(RepositoryConnection repCon = rep.getConnection()) {
            for (Statement statement : model) {
                queryModel = QueryResults.asModel(repCon.getStatements(statement.getSubject(), title, null, false));
                for (Statement s:queryModel){
                    name = s.getObject().stringValue();
                }

                if (!out.contains(name)) {
                    out.add(name);
                }
            }
        }
        return out;

    }


    public HashMap getStatementMap(Model model){

        HashMap out = new HashMap();

        String fabio = "http://purl.org/spar/fabio/";
        String dcterms = "http://purl.org/dc/terms/";
        model.setNamespace("fabio", fabio);
        model.setNamespace("dcterms",dcterms);

        ValueFactory vf = SimpleValueFactory.getInstance();

        IRI artisticWork = vf.createIRI(fabio, "ArtisticWork");
        IRI title = vf.createIRI(dcterms,"title");
        IRI creator = vf.createIRI(dcterms,"creator");
        IRI issued = vf.createIRI(dcterms,"issued");

        TupleQueryResult queryTRes =null;
        Statement queryStatement = null;

        for(Statement statement:model){
            System.out.println(statement);
            //if(statement.getPredicate().equals(RDF.TYPE) && statement.getObject().equals(artisticWork)){
            //    out.put("Name", ((MemIRI) statement.getSubject()).getLocalName());
            //}
            if(statement.getPredicate().equals(title)){
                out.put("Name", statement.getObject().stringValue());
            }
            else if (statement.getPredicate().equals(creator)){
                out.put("Author", ((MemIRI) statement.getObject()).getLocalName());
            }
            else if(statement.getPredicate().equals(issued)){
                System.out.println("ANNO");
                //Optional<Literal> year = Models.getPropertyLiteral(model, statement.getSubject(), statement.getPredicate());
                //System.out.println(year);
                //if(year != null){
                //    out.put("PublicationYear", year.get());
                //}
                out.put("PublicationYear", statement.getObject().stringValue());
            }
        }

        return out;

    }


    public void insertData(String subject, String predicate, String object ){
        String base = "<http://purl.org/spar/fabio/";
        String dcterms = "<http://purl.org/dc/terms/";
        String prismstd = "<http://prismstandard.org/namespaces/basic/2.0/>";

        try(RepositoryConnection repCon = rep.getConnection()) {

            if (subject != null) {
                subject = base + getIRICase(subject) + ">";
            }
            if (predicate != null) {
                //@TODO enum per "<http://purl.org/dc/terms/>"
                if(predicate.equals("publicationDate")) {
                    predicate = prismstd+predicate+">";
                    if (object != null) {
                        object ="\""+object+"\"^^xsd:Date" + ">";
                    }
                }
                else if(predicate.equals("hasPublicationYear")){
                    predicate = base+predicate+">";
                    if (object != null) {
                        object ="\""+object+"\"^^xsd:gYear >";
                        System.out.println(object);
                    }
                }
                else if(predicate.equals("title")){
                    predicate = dcterms+predicate+">";
                    if (object != null) {
                        object ="\""+toCamelCase(object)+"\">";
                        System.out.println(object);
                    }
                }


            } else {
                System.out.println("errore predicato");
            }

            //System.out.println("GESTORE REPO INSERT");
            //System.out.println(subject + " "+ predicate + " "+object);

            // Preparo un update per aggiungere i dati usando SPARQL 1.1
            Update update = repCon.prepareUpdate("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                    + "INSERT DATA {\n"
                    + subject + " " + predicate + " " + object + ".\n" + "}");
            // eseguo l'update
            update.execute();
            repCon.close();
        }


    }




    public void insertStatement(String subject, String predicate, String object){
        String base = "<http://purl.org/spar/fabio/";
        String dcterms = "<http://purl.org/dc/terms/";
        String dbo = "<http://dbpedia.org/ontology/";

        try(RepositoryConnection repCon = rep.getConnection()) {

            if (subject != null) {
                subject = base + getIRICase(subject) + ">";
            }
            if (predicate != null) {
                if(predicate.equals("creator")) {
                    predicate = dcterms+predicate+">";
                }

                else{
                    predicate = base + getIRICase(predicate) + ">";
                }

            } else {
                predicate = "a";
            }
            if (object != null) {
                if(object.equals("Person")){
                    object = dbo+object+">";
                }else {
                    object = base + getIRICase(object) + ">";
                }
            }
            //System.out.println("GESTORE REPO INSERT");
            //System.out.println(subject + " "+ predicate + " "+object);

            // Preparo un update per aggiungere i dati usando SPARQL 1.1
            Update update = repCon.prepareUpdate("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                    + "INSERT DATA {\n"
                    + subject + " " + predicate + " " + object + ".\n" + "}");
            // eseguo l'update
            update.execute();
            repCon.close();
        }

    }

    ////////////////////////////////////////
    ///////////////////////////////////////
    //////////////////////////////////////

    public void closeRepo(){
        rep.shutDown();
    }

    public Repository getRep() {
        return rep;
    }

    public void setRep(Repository rep) {
        this.rep = rep;
    }


    public String getIRICase(String s){
        String[] subjects = s.split(" ");
        String subject = "";
        String tmp = "";
        if(subjects.length>1) {
            for (int i = 0; i < subjects.length; i++) {
                if (subjects[i].length() > 1) {
                    tmp = subjects[i].substring(0, 1).toUpperCase() + subjects[i].substring(1, subjects[i].length()).toLowerCase();
                } else {
                    tmp = subjects[i].toUpperCase();
                }
                subject = subject + tmp;
            }
        }
        else{

            subject = subjects[0];

        }
        subject = subject.replaceAll("[^a-zA-Z0-9_-]", "");
        return subject;
    }

    public String toCamelCase(String s){
        String[] split = s.split(" ");
        String out = "";
        String tmp = "";
        if(split.length>1){
            for(int i =0; i<split.length;i++){
                if (split[i].length() > 1) {
                    tmp = split[i].substring(0, 1).toUpperCase() + split[i].substring(1, split[i].length()).toLowerCase();
                } else {
                    tmp = split[i].toUpperCase();
                }
                if(out!="") {
                    out = out + " " + tmp;
                }
                else{
                    out = tmp;
                }
            }
        }
        else{
            if(split[0].length()>1) {
                out = split[0].substring(0, 1).toUpperCase() + split[0].substring(1, split[0].length()).toLowerCase();
            }else{
                out = split[0].toUpperCase();
            }
        }
        return out;
    }


}
